﻿using RestSharp;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;


namespace LoadTesting
{
    public class Test
    {
        public string Host;
        public string Port;
        public string Path;
        public string RequestMethod;
        public int ThreadAmount = 0;
        public int RequestAmount = 0;
        public RestRequest request;
        public RestClient client;
        public int Delay;
        public int DelayThreads;
        public async Task Start()
        {
            
            Task task = new Task(() =>
             {
                for (int j = 0; j < this.RequestAmount; j++)
                {
                    try
                    {
                        Stopwatch SW = new Stopwatch();
                        var cancellationTokenSource = new CancellationTokenSource();
                        SW.Start();
                        var response = client.Execute(request); ;

                        SW.Stop();
                        Console.WriteLine($":{j} for https://{Host}:{Port}/{Path} Done {Convert.ToString(SW.Elapsed)} --- {response.StatusCode}");
                        SW = null;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    Thread.Sleep(Delay);
                }

                Console.WriteLine("Thread done");
             });
             task.Start();
        }
    }

    class Program
    {
        static async Task Main(string[] args)
        {
            Test test = new Test();

            Console.WriteLine("Hello !");

            Console.WriteLine("Please, specify host:");
            test.Host = Console.ReadLine();
            Console.WriteLine("Please, specify port:");
            test.Port = Console.ReadLine();
            Console.WriteLine("Please, specify path:");
            test.Path = Console.ReadLine();
           /* Console.WriteLine("Please, specify type of request(GET/POST/PUT/DELETE):");
            test.RequestMethod = Console.ReadLine();*/
            Console.WriteLine("Please, specify amount of active threads:");
            test.ThreadAmount = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Please, specify amount of request per thread:");
            test.RequestAmount = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Please, specify delay in miliseconds between requests:");
            test.Delay = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Please, specify delay in miliseconds between Threads:");
            test.DelayThreads = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Start test (Y/N)");
            if (Console.ReadLine() == "Y")
            {
                test.client = new RestClient($"https://{test.Host}:{test.Port}/{test.Path}"); //:{test.Port}
                test.request = new RestRequest(/*test.RequestMethod*/Method.GET);

                for (int i = 0; i < test.ThreadAmount; i++)
                {
                    await test.Start();
                    Console.WriteLine($"Thread:{i} started...");
                    Thread.Sleep(test.DelayThreads);
                }
            }

            Console.ReadLine();
        }
    }
}

   
